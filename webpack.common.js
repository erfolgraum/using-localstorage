const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");


module.exports = {

    entry: path.resolve(__dirname, "src", "main.js"),

    output: {
      path: path.resolve(__dirname, "build"),
      clean: true,
      filename: "bundle.js",
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "src", "index.html"),
      }),
      new MiniCssExtractPlugin({
        filename: "[name]-[contenthash].css",
      }),

      new CopyPlugin({
        patterns: [
          {
            from: "src/assets/images/*.png",
            to({ context }) {
              return "./[name][ext]";
            },
          },
        ],
      }),
    ],

    module: {
      rules: [
        // { test: /\.(js)$/, 
        // exclude: /node_modules/, 
        // use: "babel-loader" },
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader"],
        },
        // {
        //   test: /\.jpg$/,
        //   loader: "file-loader",
        //   options: {
        //     name: '[name].[ext]',
        //     outputPath: 'images',
        //   },
        // },
      ],
    },
};
