import { email } from "./join-us-section.js";


const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

// let regex = new RegExp("[a-z0-9]+@(gmail|outlook|ukr)+\.[a-z]{2,3}$");
export let regex = new RegExp("[a-z0-9]+@(gmail\.com|outlook\.com|yandex\.ru)$");

export function validate(email) {
    
  for(let address of VALID_EMAIL_ENDINGS) {
        if(email.includes(address) && regex.test(email)){         
            return true
        } 
  }  
  
  return false
}

export function storeToLocalStorage(savedEmail) {
  for(let address of VALID_EMAIL_ENDINGS) {
        if(savedEmail.includes(address) && regex.test(savedEmail)){
            return  localStorage.setItem('email', savedEmail)
        }     
  }
  return false  
}