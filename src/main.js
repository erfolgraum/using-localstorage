import { creator, email, form, btn } from "./join-us-section.js";
import { storeToLocalStorage } from "./email-validator.js";

/* eslint no-unused-vars: "off" */
/* eslint quotes: ["error", "single"] */
/* eslint prefer-arrow-callback: "off" */
/* eslint func-names: "off" */
document.addEventListener("DOMContentLoaded", function () {
  // creator.create('advanced')
  creator.create("standard");
  // creator.create().remove()

  form.addEventListener("input", (e) => {
    e.preventDefault();
    // const nameOnly = savedEmail.substring(0, savedEmail.indexOf("@"));
    storeToLocalStorage(e.target.value);
  });

  window.addEventListener("DOMContentLoaded", function (e) {
    if (localStorage.getItem("email") === null) {
      e.target.value = "";
      email.style.display = "inline-block"; 
    }
    else {
        email.style.display = "none"
      email.value = localStorage.getItem("email");
      btn.setAttribute("value", "Unsubscribe");
    }
  });
});
